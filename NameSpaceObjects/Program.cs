﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HospitalStructure;
using Automobiles;
using Children;

namespace NameSpaceObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            //For HospitalStructure Namespace
            Patient patient1 = new Patient("Emeka", "Joseph", 25, "AA", "0-");
            Patient patient2 = new Patient("Joan", "Victor", 33, "AA", "0+");
            Patient patient3 = new Patient("Isaac", "Ogunlala", 44, "AS", "AB");
            Patient patient4 = new Patient("Success", "Mogbe", 18, "AA", "0-");
            Patient[] patients = {patient1, patient2, patient3, patient4 };
            Hospital myHospital = new Hospital(patients,"Prince and Pricess Hospital");

            myHospital.treatPatient(patient1);
            Hospital.dischargePatient(patient4);

            Console.WriteLine(myHospital.paintBuilding() + "\n\n");


            //For Automobile Namespace
            Motor Honda = new Motor();
            Honda.accelerate();
            Honda.accelerate();
            Honda.accelerate();
            Honda.accelerate();
            Honda.accelerate();
            Honda.accelerate();
            Honda.accelerate();
            Honda.accelerate();
            Honda.accelerate();
            Honda.stop();

            tricycle keke = new tricycle();
            keke.decelarate();
            Console.WriteLine("\n\n");


            //For Children NameSpace
            Student serena = new Student();
            serena.setFirstName("Serena");
            serena.setLastName("Eze");

            Teacher Katule = new Teacher();
            Katule.setFirstName("Jeffrey");
            Katule.setLastName("Katule");

            Katule.teachStudent(serena);


            Console.ReadKey();
        }
    }
}

namespace HospitalStructure
{
    public class Building
    {
        protected int numberOfStories;
        protected int numberOfRooms;

        public string paintBuilding()
        {
            return "Building painted";
        }
    }
    
    public class Hospital:Building
    {
        private string nameOfHospital;
        private Patient[] patients;

        public Hospital(Patient[] patients, string hospitalName)
        {
            this.patients = patients;
            this.nameOfHospital = hospitalName;
        }

        public void treatPatient(Patient p)
        {
            Console.WriteLine(p.getName() + " has been treated successfully!!! ");
        }
        public string BillPatient(Patient p)
        {
            double amount = 5000.00;
            return "Your Bill is: " + amount + "Naira only.\n" + nameOfHospital+": Health is wealth!!!";
        }
        public static void dischargePatient(Patient p)
        {
            Console.WriteLine("Your are free to go " + p.getName() + "\nWish you quick recovery. \nThank you for believing us");
        }
        public void setNameOfHospital(string name)
        {
            nameOfHospital = name;
        }
        public string getNameOfHospital()
        {
            return this.nameOfHospital;
        }
    }

    public class Patient
    {
        private string firstName;
        private string lastName;
        private int age;
        private string genoType;
        private string bloodGroup;

        public Patient(string first, string last, int age, string genoType, string bloodGroup)
        {
            this.firstName = first;
            this.lastName = last;
            this.age = age;
            this.genoType = genoType;
            this.bloodGroup = bloodGroup;
        }

        public string getGenoType()
        {
            return this.genoType;
        }
        public string getBloodGroup()
        {
            return this.bloodGroup;
        }
        public string getName()
        {
            return this.lastName + " " + this.firstName;
        }
        public int getAge()
        {
            return this.age;
        }
    }

}

namespace Automobiles
{
    public class Vehicle
    {
        protected int speed = 0;

        public void accelerate()
        {
            if(speed > 120)
            {
                speed = 140;
                Console.WriteLine("Speed Limit reached!!!");
            }
            else if (speed < 140)
            {
                speed = speed + 20;
            }
            Console.WriteLine("Your speed is now " + speed + "km/hr");
        }

        public void decelarate()
        {
            if(speed < 20)
            {
                speed = 0;
                Console.WriteLine("Can't go any lower than this");
            }
            
            else if(speed > 0 )
            {
                speed = speed - 20;
            }
        }
        public void stop()
        {
            speed = 0;
            Console.WriteLine("Vehicle stopped");
        }
        public int getSpeed()
        {
            return this.speed;
        }
    }
    public class Motor:Vehicle
    {
        private int numberOfSeat;
        
        public void setNumberOfString(int seats)
        {
            numberOfSeat = seats;
        }
        public int getNumberOfSeat()
        {
            return numberOfSeat;
        }
    }
    public class tricycle:Vehicle
    {
        private const int numberOfSeat = 2;

        public int getNumberOfSeat()
        {
            return numberOfSeat;
        }

    }
}

namespace Children
{
    public class Child
    {
        protected string firstName;
        protected string lastName;
        protected int age;
        protected int weight;


        public void cry()
        {
            Console.WriteLine("aaahhhhhhh!!!!!!");
        }

        public void laugh()
        {
            Console.WriteLine("Hahahahahahaha");
        }

        public string eat(string food)
        {
            return "Finished eating " + food;
        }

        public void setFirstName(string Name)
        {
            this.firstName = Name;
        }
        public void pop()
        {
            if (weight >= 10)
            {
                weight = weight - 5;
                Console.WriteLine("Hurry up, am done here!!!");
            }
        }

        public void setLastName(string Name)
        {
            this.lastName = Name;
        }
        public void setAge(int age)
        {
            this.age = age;
        }
        public int getAge()
        {
            return this.age;
        }

        public string getName()
        {
            return this.lastName + " " + this.firstName;
        }
    }

    public class Student : Child
    {
        private int regNumber;
        private int level;
        private bool isMale;

        public void setRegNumber(int reg)
        {
            this.regNumber = reg;
        }
        public int getRegNumber()
        {
            return this.regNumber;
        }

        public void setLevel(int level)
        {
            this.level = level;
        }
        public int getLevel()
        {
            return this.level;
        }
        public void isMaleStudent(bool isMale)
        {
            this.isMale = isMale;
        }
        public bool getSex()
        {
            return this.isMale;
        }
    }

    public class Teacher:Child
    {
        public void teachStudent(Student a)
        {
            Console.WriteLine("That will all " + a.getName() + "\nYou can go now!!!");
        }
    }

}

